import Foundation

class Calculator {
    
    private static var instance: Calculator? = nil
    
    private init() { }
    
    public static func getInstance() -> Calculator? {
        if instance == nil {
            instance = Calculator()
        }
        
        return instance
    }
    
    func mul(r1: Rational, r2: Rational) -> Rational {
        let num = r1.numerator * r2.numerator
        let den = r1.denominator * r2.denominator
        return Rational(num, den)
    }
    
    func div(r1: Rational, r2: Rational) -> Rational {
        let num = r1.numerator * r2.denominator
        let den = r1.denominator * r2.numerator
        return Rational(num, den)
    }
    
}

class Rational {
    
    var numerator: Int = 0
    var denominator: Int = 1
    
    init(_ num: Int,_ den: Int){
        numerator = num
        denominator = den
    }
    
    func add(r: Rational) -> Rational {
        var l = 0
        var newNumerator = 0
        
        var num1 = self.numerator
        var num2 = r.numerator
        var den1 = self.denominator
        var den2 = r.denominator
        
        if den1 != den2 {
            l = lcm(den1, den2)
            
            var mult1 = l / den1
            var mult2 = l / den2
            
            newNumerator = (num1 * mult1) + (num2 * mult2)
            
        } else {
            l = den1
            newNumerator = num1 + num2
        }
        
        
        return Rational(newNumerator,l)
    }
    
    func sub(r: Rational) -> Rational {
        var l = 0
        var newNumerator = 0
        
        var num1 = self.numerator
        var num2 = r.numerator
        var den1 = self.denominator
        var den2 = r.denominator
        
        if den1 != den2 {
            l = lcm(den1, den2)
            
            var mult1 = l / den1
            var mult2 = l / den2
            
            newNumerator = (num1 * mult1) - (num2 * mult2)
            
        } else {
            l = den1
            newNumerator = num1 - num2
        }
        
        
        return Rational(newNumerator,l)
    }
    
    func gcd(_ a: Int, _ b: Int) -> Int {
        let r = a % b
        if r != 0 {
            return gcd(b, r)
        } else {
            return b
        }
    }
    
    func lcm(_ m: Int, _ n: Int) -> Int {
        return m / gcd(m, n) * n
    }

}

let calculator = Calculator.getInstance()

let rational1  = Rational(3,5)
let rational2  = Rational(2,3)
let rational3  = Rational(1,5)

let result = calculator?.mul(r1: rational1, r2: rational2).sub(r: rational3)
print("Result \(result?.numerator) / \(result?.denominator)")



