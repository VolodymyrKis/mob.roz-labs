
import Foundation

//Zavd1
//1
let fibArray = [ 0, 1, 1, 2, 3, 5, 8, 13, 21, 34 ]

//2
let revArray = fibArray.sorted(by: >)
print(revArray)

//3
let snglArray = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]

//4
print(snglArray.count)

//5
print(snglArray[9])

//6
print(snglArray[15...20])

//7
let rvalue = snglArray[9]
var rptArray = [Int]()

for _ in 0..<10 {
    rptArray.append(rvalue)
}

var rptArray2 = Array(repeating: rvalue, count: 10)

print(rptArray)
print(rptArray2)
/////////////////////////

//8
var oddArray = [1, 3, 5, 7, 9]

//9
oddArray.append(11)
print(oddArray)

//10
oddArray += [15, 17, 19]
print(oddArray)

//11
oddArray.insert(13, at: 6)
print(oddArray)

//12
oddArray.removeSubrange(4...6)
print(oddArray)

//13
print(oddArray.removeLast())

//14
oddArray.replaceSubrange(1...oddArray.count-1, with: [2, 3, 4])
print(oddArray)

//15
oddArray.remove(at: 2)
print(oddArray)

//16
print(oddArray.contains(3))

//17
print(oddArray)

//Zavd2
//////////////////////////////
//1
let chSet: Set = ["a", "b", "c", "d"]

//2
var mChSet: Set = chSet

//3
print(mChSet.count)

//4
mChSet.insert("e")
mChSet.insert("f")
print(mChSet)

//5
var srtChSet = mChSet.sorted()
print(srtChSet)

//6
print(mChSet.remove("f")!)
print(mChSet)

//7
mChSet.remove(at: mChSet.index(of: "d")!)
print(mChSet)

//8
print(mChSet.distance(from: mChSet.index(of: mChSet.first!)!, to: mChSet.index(of: "a")!))

//9
mChSet.insert("a")
print(mChSet)

//10
var aSet: Set = ["One", "Two", "Three", "1", "2"]
var bSet: Set = ["1", "2", "3", "One", "Two"]

//11
let abSet = aSet.intersection(bSet)
print(abSet)

//12
var uniqueAB = aSet.subtracting(bSet)
var uniqueBA = bSet.subtracting(aSet)
print(uniqueAB)
print(uniqueBA)

//13
var notSame = aSet.symmetricDifference(bSet)
print(notSame)

//14
var unionAB = aSet.union(bSet)
print(unionAB)

//15
var xSet: Set = [2, 3, 4]
var ySet: Set = [1, 2, 3, 4, 5, 6]
var zSet: Set = [3, 4, 2]
var x1Set: Set = [5, 6, 7]

//16
print(xSet.isSubset(of: ySet))
print(ySet.isSubset(of: xSet))

//17
print(xSet.isSuperset(of: ySet))
print(ySet.isSuperset(of: xSet))

//18
print(xSet == zSet)

//19
print(xSet.isStrictSubset(of: zSet))

//20
print(xSet.isStrictSuperset(of: zSet))


//Zavd3
//1
let nDict = [1: "One", 2: "Two", 3: "Three", 4: "Four", 5: "Five"]

//2
print(nDict[3])

//3
print(nDict[4])

//4
var mDict = nDict

//5
mDict[6] = "Seven"
mDict[7] = "Six"

//6
mDict.updateValue("Six", forKey: 6)
mDict.updateValue("Seven", forKey: 7)
mDict.updateValue("Eight", forKey: 8)

//7
mDict.removeValue(forKey: 5)

//8

//9
//mDict.distance(from: 0, to: 6)


//10
var mDictKeys = mDict.keys

//11
var mDictValues = mDict.values


//12
print(mDict.count)
print(mDict.keys.count)
print(mDict.values.count)

//13
print(mDict)



